# MANUAL DE USUARIO - PERFIL RECEPCIÓN
El usuario puede:

* Cambiar contraseña y actualizar su información personal.
* Registrar a visistantes.
* Consultar a los visitantes activos.
* Registrar las salidas de los visitantes.
* Ver el historial de visitantes.

## 1. GESTIÓN DEL DOCUMENTO

| | Elaboración | Revisión | Aprobación |
| ---: | :---: | :---: | :---: |
| **Firma** | | | |
| **Nombre** | Jhonny Ruben Monrroy Casillo | Richar Angel Marquez Zacari | Hans Wilmer Choque Gutierrez |
| **Cargo** | __Consultor - Desarrollo de Servicios de Verificación__ | __Jefe de la Unidad Nacional de Implementación de Sistemas__ | __Director Nacional de Tecnologías de la Información__ |
| **Fecha** | 19/08/2022 | | |

## 2. OBJETO DEL DOCUMENTO

Mediante el presente documento se proporciona información necesaria que debe ser tomada en cuenta por el usuario de Perfil Control de Ingresos a la Entidad que hará uso del Sistema de Control de Accesos - DNTIC-SEGIP.

## 3. ÁMBITO DE APLICACIÓN

Se aplica a todos los servidores públicos que tienen como labor principal registrar el acceso y salidas de visitas a la institución.

## 4. DEFINICIONES

| Definición | Descripción |
| :---: | :--- |
| SCA | Sistema de Control de Accesos. |

## 5. DOCUMENTOS DE REFERENCIA

| Códido | Título |
| :---: | :---: |
| N/A | |

## 6. MANUAL

### 6.1. INICIO DE SESIÓN

#### Ingresar al sistema SCA

Utilice su usuario y password para autenticarse al sistema.

![](images/man_001.png)

#### Reconociendo la interfaz de usuario

![](images/man_002.png)

### 6.2. CAMBIAR CONTRASEÑA

#### Paso 1
Hacer click en el icono Principal

![](images/man_003.png)

En la pantalla principal hacer click la opción _"Cambiar Clave"_

![](images/man_004.png)

#### Paso 2
Introducir su contraseña actual, nueva contraseña y presione el boton _Cambiar_.

![](images/man_005.png)

A continuación aparecerá el mensaje de confirmación de que su contraseña fue cambiada exitosamente.

![](images/man_006.png)

### 6.3. ACTUALIZAR INFORMACIÓN PERSONAL

#### Paso 1

Hacer click en el icono Principal

![](images/man_003.png)

En la pantalla principal hacer click la opción _"Usuario"_

![](images/man_007.png)

#### Paso 2

Cambiar su información personal segun corresponda, y presione en boton _"Actualizar"_ para guardar los cambios.

![](images/man_008.png)

A continuación aparecerá el mensaje de confirmación de que sus datos se actualizaron exitosamente.

![](images/man_009.png)

### 6.4. REGISTRAR VISITANTES

#### Paso 1

Hacer click en el icono _"Incluir Visitantes"_

![](images/man_010.png)

#### Paso 2

Introducir el Nro del documento que presento el visitante al momento de ingresar a la entidad.

![](images/man_011.png)

#### Paso 3

Completar la información del visitante y presionar el boton _"Siguiente"_

![](images/man_012.png)

#### Paso 4

Asignarle una ficha de ingreso y registrarlo en el sistema como visitante con el boton _"Registrar Visitante"_

![](images/man_013.png)

Inmediatamente la visita será registrada y aparecerá el listado de todos los visitantes activos en la institución.

![](images/man_014.png)

### 6.5. CONSULTAR VISITANTES

#### Paso 1

Hacer click en el icono _"Consulta Visitantes"_

![](images/man_015.png)

A continuación aparecerá un listado de los visitantes activos.

![](images/man_016.png)

#### Paso 2

Si se desea generar un reporte de los visitantes actuales haga click en la opción _"Generar"_.

![](images/man_017.png)

A continuación se generará un documento PDF que podra posteriormente imprimirlo si lo desea.

![](images/man_018.png)

![](images/man_019.png)

### 6.6. REGISTRAR SALIDA DE VISITANTES

#### Paso 1

Hacer click en el icono _"Salida Visitantes"_

![](images/man_020.png)

#### Paso 2

Introducir el Nro del ticket del visitante y pulsar el boton _"Buscar"_

![](images/man_021.png)

#### Paso 3

Se listara el visitante buscado. Para registrar su salida y liberar el ticket para utilizarlo en otro visitante, presione en el boton _"Registrar Salida"_.

![](images/man_022.png)

Aparecerá un mensaje de confirmación de que se registro la salida del visitante.

![](images/man_023.png)

### 6.7. VER EL HISTORIAL DE VISITAS

#### Paso 1 

Hacer click en el icono _"Historial de Visitas"_

![](images/man_024.png)

#### Paso 2

En el listado de visitas se puede generar un reporte en archivo PDF, realizar una busqueda por tipo de visitante y rango de fechas.

![](images/man_025.png)

#### Paso 3

Si se desea generar un reporte del historial de visitantes haga click en la opción _"Generar PDF"_.

![](images/man_026.png)

A continuación se generará un documento PDF que podra posteriormente imprimirlo si lo desea.

![](images/man_018.png)

![](images/man_027.png)

### 6.8. CERRAR SESIÓN

#### Paso 1 

Hacer click en el icono _"Cerrar Sesión"_

![](images/man_028.png)

A continuación se mostrará el inicio de sesión del sistema.

![](images/man_029.png)

## 9. REGISTROS

| Código | Título |
| :---: | :---: |
| N/A | |

## 10. CONTROL DE CAMBIOS

| Versión | Fecha de modificación | Cambios de modificación |
| :---: | :---: | :--- |
| 00 | 22/08/2022 | Versión inicial del documento @jhonny.monrroy |

