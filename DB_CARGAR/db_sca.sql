-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 24-06-2022 a las 15:01:34
-- Versión del servidor: 5.7.15-log
-- Versión de PHP: 5.6.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_sca`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `anexos_t`
--

CREATE TABLE `anexos_t` (
  `id_anexo_t` int(11) NOT NULL,
  `anexo_t` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `anexos_t`
--

INSERT INTO `anexos_t` (`id_anexo_t`, `anexo_t`) VALUES
(0, '---seleccione---'),
(1, 'Ninguno'),
(2, 'Sobre Cerrado'),
(3, 'Paquete');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asuntos`
--

CREATE TABLE `asuntos` (
  `id_asunto` int(11) NOT NULL,
  `asunto` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `asuntos`
--

INSERT INTO `asuntos` (`id_asunto`, `asunto`) VALUES
(0, '---seleccione---'),
(1, 'Reunion de Trabajo'),
(3, 'Solicitud Informacion'),
(4, 'Entrega de Factura de Servicio'),
(5, 'Atender Asunto Personal'),
(6, 'Entrega de Documentacion'),
(7, 'Retirar Documentacion'),
(8, 'Investigacion'),
(9, 'Entrega de Valores'),
(10, 'Asistir a Rueda de Prensa'),
(11, 'Visita'),
(12, 'Taller');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bases_db`
--

CREATE TABLE `bases_db` (
  `base_db` varchar(255) NOT NULL,
  `id_base_db_t` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bases_db_t`
--

CREATE TABLE `bases_db_t` (
  `id_base_db_t` int(11) NOT NULL,
  `base_db_t` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `bases_db_t`
--

INSERT INTO `bases_db_t` (`id_base_db_t`, `base_db_t`) VALUES
(0, '---seleccione---'),
(1, 'Sigesp Principal'),
(2, 'Sigesp Secundario'),
(3, 'SISYC');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `correspondencias`
--

CREATE TABLE `correspondencias` (
  `id_correspondencia` int(11) NOT NULL,
  `id_correspondencia_t` int(11) NOT NULL,
  `id_anexo_t` int(11) NOT NULL,
  `cod_procedencia` varchar(255) NOT NULL,
  `procedencia` varchar(255) NOT NULL,
  `destino_nombre` varchar(255) NOT NULL,
  `destino_cargo` varchar(255) NOT NULL,
  `cod_emisor` varchar(255) NOT NULL,
  `emisor` varchar(255) NOT NULL,
  `emisor_nombre` varchar(255) NOT NULL,
  `emisor_cargo` varchar(255) NOT NULL,
  `receptor_nombre` varchar(255) NOT NULL,
  `fecha_r` datetime NOT NULL COMMENT 'Fecha Recepcion',
  `fecha_e` datetime NOT NULL COMMENT 'Fecha de Entrega'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `correspondencias_t`
--

CREATE TABLE `correspondencias_t` (
  `id_correspondencia_t` int(11) NOT NULL,
  `correspondencia_t` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `correspondencias_t`
--

INSERT INTO `correspondencias_t` (`id_correspondencia_t`, `correspondencia_t`) VALUES
(0, '---seleccione---'),
(1, 'Oficio');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empcp`
--

CREATE TABLE `empcp` (
  `cod_empcp` varchar(100) NOT NULL,
  `cod_ent` varchar(100) NOT NULL,
  `entidad` varchar(100) NOT NULL,
  `cod_mun` varchar(100) NOT NULL,
  `municipio` varchar(100) NOT NULL,
  `cod_parr` varchar(100) NOT NULL,
  `parroquia` varchar(100) NOT NULL,
  `cod_cp` varchar(100) NOT NULL,
  `centro_pob` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empcp`
--

INSERT INTO `empcp` (`cod_empcp`, `cod_ent`, `entidad`, `cod_mun`, `municipio`, `cod_parr`, `parroquia`, `cod_cp`, `centro_pob`) VALUES
('010101001', '01', 'H.A.M. Oruro', '01', 'La Paz', '01', 'Oruro', '001', 'Oruro (Capital)\r'),
('010102001', '02', 'H.A.M. La Paz', '01', 'La Paz', '02', 'La Paz', '001', 'La Paz (Capital)\r'),
('010103001', '03', 'H.A.M. El Alto', '01', 'La Paz', '03', 'La Paz', '001', 'El Alto (Capital)\r'),
('010104001', '04', 'H.A.M. Cochabamba', '01', 'Cochabamba', '04', 'Cochabamba', '001', 'Cochabamba (Capital)\r'),
('010105001', '05', 'H.A.M. Santa Cruz', '01', 'Santa Cruz', '05', 'Santa Cruz', '001', 'Santa Cruz (Capital)\r'),
('010106001', '06', 'H.A.M. Beni', '01', 'Beni', '06', 'Beni', '001', 'Beni (Capital)\r'),
('010107001', '07', 'H.A.M. Tarija', '01', 'Tarija', '07', 'Tarija', '001', 'Tarija (Capital)\r'),
('010107002', '08', 'H.A.M. Potosí', '01', 'Potosí', '07', 'Potosí', '002', 'Potosí (Capital)\r'),
('010107003', '09', 'H.A.M. Pando', '01', 'Pando', '07', 'Pando', '003', 'Pando (Capital)\r'),
('010107004', '10', 'H.A.M. Sucre', '01', 'Sucre', '07', 'Sucre', '004', 'Sucre (Capital)\r'),
('010107007', '11', 'Ministerio de Autonomias', '01', 'La Paz', '07', 'La Paz', '007', 'La Paz (Capital)\r'),
('010108001', '12', 'Ministerio de Defensa', '01', 'La Paz', '08', 'La Paz', '001', 'La Paz (Capital)\r'),
('010109001', '13', 'Ministerio de Desarrollo Rural y Tierras', '01', 'La Paz', '09', 'La Paz', '001', 'La Paz (Capital)\r'),
('010110001', '14', 'Ministerio de Economía y Finanzas Públicas', '01', 'La Paz', '10', 'La Paz', '001', 'La Paz (Capital)\r'),
('010111001', '15', 'Ministerio de Educación', '01', 'La Paz', '11', 'La Paz', '001', 'La Paz (Capital)\r'),
('010112001', '16', 'Ministerio de Gobierno', '01', 'La Paz', '12', 'La Paz', '001', 'La Paz (Capital)\r'),
('010113001', '17', 'Ministerio de Hidrocarburos y Enegía', '01', 'La Paz', '13', 'La Paz', '001', 'La Paz (Capital)\r'),
('010114001', '18', 'Ministerio de Justicia', '01', 'La Paz', '14', 'La Paz', '001', 'La Paz (Capital)\r'),
('010115001', '19', 'Ministerio de la Presidencia', '01', 'La Paz', '15', 'La Paz', '001', 'La Paz (Capital)\r'),
('010116001', '20', 'Ministerio de Medio Ambiente y Agua', '01', 'La Paz', '16', 'La Paz', '001', 'La Paz (Capital)\r'),
('010117001', '21', 'Ministerio de Mineria y Metalurgia', '01', 'La Paz', '17', 'La Paz', '001', 'La Paz (Capital)\r'),
('010118001', '22', 'Ministerio de Planificacion del Desarrollo', '01', 'La Paz', '18', 'La Paz', '001', 'La Paz (Capital)\r'),
('010119001', '23', 'Ministerio de Relaciones Exteriores', '01', 'La Paz', '19', 'La Paz', '001', 'La Paz (Capital)\r'),
('010120001', '24', 'Ministerio de Salud y Deportes', '01', 'La Paz', '20', 'La Paz', '001', 'La Paz (Capital)\r'),
('010121001', '27', 'Ministerio de Transparencia', '01', 'La Paz', '21', 'La Paz', '001', 'La Paz (Capital)\r'),
('010121008', '26', 'Prensa', '01', 'La Paz', '21', 'La Paz', '008', 'La Paz (Capital)\r'),
('010121010', '27', 'Otros', '01', 'La Paz', '21', 'La Paz', '010', 'Otros (Capital)\r'),
('010121011', '28', 'Otros', '01', 'La Paz', '21', 'La Paz', '010', 'Otros (Capital)');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historiales`
--

CREATE TABLE `historiales` (
  `id_historial` int(11) NOT NULL,
  `id_usuario` varchar(100) NOT NULL,
  `ip_acceso_u` varchar(100) NOT NULL DEFAULT '',
  `fecha` datetime NOT NULL,
  `id_registro` varchar(255) NOT NULL,
  `tabla` varchar(50) NOT NULL,
  `campo` varchar(100) NOT NULL,
  `contenido` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `historiales`
--

INSERT INTO `historiales` (`id_historial`, `id_usuario`, `ip_acceso_u`, `fecha`, `id_registro`, `tabla`, `campo`, `contenido`) VALUES
(428802, 'mvera', '127.0.0.1', '2022-06-10 12:46:55', 'reporte', 'visitantes_h', 'sca_visitante_r.php', 'Generacion de Reporte PDF'),
(428803, 'mvera', '127.0.0.1', '2022-06-10 12:47:12', 'reporte', 'visitantes_h', 'sca_visitante_r.php', 'Generacion de Reporte PDF'),
(428804, 'mvera', '127.0.0.1', '2022-06-10 12:47:14', 'reporte', 'visitantes_h', 'sca_visitante_r.php', 'Generacion de Reporte PDF'),
(428805, 'mvera', '127.0.0.1', '2022-06-10 12:47:28', 'reporte', 'visitantes_h', 'sca_visitante_r.php', 'Generacion de Reporte PDF'),
(428806, 'mvera', '127.0.0.1', '2022-06-10 12:47:57', 'reporte', 'visitantes_h', 'sca_visitante_historial_r.php', 'Generacion de Reporte PDF'),
(428807, 'mvera', '127.0.0.1', '2022-06-10 12:49:09', 'reporte', 'visitantes_h', 'sca_visitante_historial_r.php', 'Generacion de Reporte PDF'),
(428808, 'mvera', '::1', '2022-06-10 08:49:30', 'reporte', 'visitantes_h', 'sca_visitante_historial_r.php', 'Generacion de Reporte PDF'),
(428809, 'mvera', '::1', '2022-06-10 08:50:09', 'reporte', 'visitantes_h', 'sca_visitante_historial_r.php', 'Generacion de Reporte PDF'),
(428810, 'mvera', '::1', '2022-06-10 08:50:19', 'reporte', 'visitantes_h', 'sca_visitante_r.php', 'Generacion de Reporte PDF'),
(428811, 'mvera', '127.0.0.1', '2022-06-23 18:46:42', '56895', 'visitantes', 'Inclusi?n de visitante', 'insert into visitantes (cedula, nombre, apellido, telefono_movil, cod_ent, cod_mun, cod_parr, cod_cp, direccion, id_visitante_t) values (\'56895\', \'reynaldo\', \'mamani\', \'7411455\', \'18\', \'\', \'\', \'\', \'\', \'9\')'),
(428812, 'mvera', '127.0.0.1', '2022-06-23 18:46:42', '107028', 'visitantes_h', 'Inclusi?n de visitantes_h', 'insert into visitantes_h (cedula, cod_procedencia, procedencia, cod_emisor, emisor, fecha_e, id_asunto, observacion, id_carnet) values (\'56895\', \'\', \'depa la paz\', \'\', \'desarrollo\', \'2022-06-23 18:46:42\', \'8\', \'SISTEMA DE RRHH\', \'1\')'),
(428813, 'mvera', '127.0.0.1', '2022-06-23 18:46:42', '107028', 'visitantes_a', 'Inclusi?n de visitantes_a', 'insert into visitantes_a (id_visitante_h, codemp, autorizacion_nombre, codper, cedper, nomper, apeper, minorguniadm, ofiuniadm, uniuniadm, prouniadm, depuniadm, desuniadm, base_db) values (\'107028\', \'\', \'santos\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\')'),
(428814, 'mvera', '127.0.0.1', '2022-06-23 18:47:00', '1', 'visitantes_h', 'fecha_s', '2022-06-23 18:46:56'),
(428815, 'mvera', '127.0.0.1', '2022-06-23 18:47:13', 'reporte', 'visitantes_h', 'sca_visitante_historial_r.php', 'Generacion de Reporte PDF');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historiales_m`
--

CREATE TABLE `historiales_m` (
  `id_historial_m` int(11) NOT NULL,
  `id_usuario` varchar(100) NOT NULL,
  `url` varchar(255) NOT NULL,
  `fecha_r` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Historiales de Modulos';

--
-- Volcado de datos para la tabla `historiales_m`
--

INSERT INTO `historiales_m` (`id_historial_m`, `id_usuario`, `url`, `fecha_r`) VALUES
(1, 'demo2', '/sca_limpio/sca/arbol/p_arbol_emisores.php', '2010-12-10 14:50:00'),
(2, 'demo', '/sca_limpio/sca/arbol/p_arbol_emisores.php', '2010-12-10 14:53:52'),
(3, 'demo2', '/sca_limpio/sca/arbol/p_arbol_emisores.php', '2010-12-10 14:55:00'),
(4, 'demo2', '/sca_limpio/sca/arbol/p_arbol_emisores.php', '2010-12-10 15:26:16'),
(5, 'demo2', '/sca_limpio/sca/arbol/p_arbol_emisores.php', '2010-12-10 15:26:21'),
(6, 'demo2', '/sca_limpio/sca/arbol/p_arbol_emisores.php', '2010-12-10 15:28:18'),
(7, 'demo2', '/sca/arbol/p_arbol_emisores.php', '2011-09-22 11:10:56'),
(8, 'demo2', '/sca/arbol/p_arbol_emisores.php', '2011-09-22 11:10:56'),
(9, 'demo2', '/sca/arbol/p_arbol_emisores.php', '2011-09-22 11:10:58'),
(10, 'demo2', '/sca/arbol/p_arbol_emisores.php', '2011-09-22 11:11:15'),
(11, 'demo2', '/sca/arbol/p_arbol_emisores.php', '2011-09-22 11:11:49'),
(12, 'demo2', '/sca/arbol/p_arbol_emisores.php', '2011-09-22 11:12:08'),
(13, 'demo2', '/sca/arbol/p_arbol_emisores.php', '2011-10-03 11:09:18'),
(14, 'demo', '/sca/arbol/p_arbol_emisores.php', '2011-10-03 17:32:35'),
(15, 'demo2', '/sca/arbol/p_arbol_emisores.php', '2011-10-03 17:35:27'),
(16, 'demo2', '/sca/arbol/p_arbol_emisores.php', '2011-10-03 17:35:37'),
(17, 'demo2', '/sca/arbol/p_arbol_emisores.php', '2011-10-03 17:42:36'),
(18, 'demo2', '/sca/arbol/p_arbol_emisores.php', '2011-10-03 17:42:40'),
(19, 'demo2', '/sca/arbol/p_arbol_emisores.php', '2011-10-03 17:42:44'),
(20, 'demo2', '/sca/arbol/p_arbol_emisores.php', '2011-10-03 17:42:50'),
(21, 'demo2', '/sca/arbol/p_arbol_emisores.php', '2011-10-03 17:42:51'),
(22, 'demo2', '/sca/arbol/p_arbol_emisores.php', '2011-10-03 17:42:52'),
(23, 'demo2', '/sca/arbol/p_arbol_emisores.php', '2011-10-03 17:42:52'),
(24, 'demo2', '/sca/arbol/p_arbol_emisores.php', '2011-10-03 17:42:52'),
(25, 'demo2', '/sca/arbol/p_arbol_emisores.php', '2011-10-03 17:43:01'),
(26, 'demo2', '/sca/arbol/p_arbol_emisores.php', '2011-10-03 17:43:06'),
(27, 'demo2', '/sca/arbol/p_arbol_emisores.php', '2011-10-03 17:45:04'),
(28, 'demo2', '/sca/arbol/p_arbol_emisores.php', '2011-10-03 17:45:08'),
(29, 'demo2', '/sca/arbol/p_arbol_emisores.php', '2011-10-03 17:45:09'),
(30, 'demo2', '/sca/arbol/p_arbol_emisores.php', '2011-10-03 17:45:09'),
(31, 'demo2', '/sca/arbol/p_arbol_emisores.php', '2011-10-03 17:45:10'),
(32, 'demo2', '/sca/arbol/p_arbol_emisores.php', '2011-10-03 17:45:10'),
(33, 'demo2', '/sca/arbol/p_arbol_emisores.php', '2011-10-03 17:45:10'),
(34, 'demo4', '/sca/arbol/p_arbol_emisores.php', '2011-10-03 17:57:44'),
(35, 'demo4', '/sca/arbol/p_arbol_emisores.php', '2011-10-03 17:57:53'),
(36, 'vramos', '/sca/arbol/p_arbol_emisores.php', '2011-10-05 17:10:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menus`
--

CREATE TABLE `menus` (
  `id_menu` int(11) NOT NULL,
  `id_menu_p` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `id_tipo_u` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `observacion` text NOT NULL,
  `color` varchar(50) NOT NULL,
  `imagen` varchar(225) NOT NULL,
  `orden` int(11) NOT NULL,
  `no_publicado` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `menus`
--

INSERT INTO `menus` (`id_menu`, `id_menu_p`, `nombre`, `id_tipo_u`, `url`, `observacion`, `color`, `imagen`, `orden`, `no_publicado`) VALUES
(1, 0, 'Principal', 1, 'sca_principal.php', '', '', 'principal.png', 1, 0),
(2, 0, 'Asunto', 1, 'sca_submenu.php', '', '', 'asunto.png', 3, 0),
(3, 2, 'Consulta Asunto', 1, 'sca_asunto_c.php', '', '', 'consultar_asunto.png', 1, 0),
(4, 2, 'Incluir asunto', 1, 'sca_asunto_i.php', '', '', 'incluir_asunto.png', 2, 0),
(5, 0, 'Visitantes', 1, 'sca_submenu.php', '', '', 'visitantes.png', 2, 0),
(6, 5, 'Incluir Visitantes ', 1, 'sca_sno_personalnomina_c.php', '', '', 'incluir_visitantes.png', 0, 0),
(7, 5, 'Consulta Visitantes', 1, 'sca_visitante_c.php', '', '', 'consulta_visitantes.png', 1, 0),
(8, 5, 'Salida Visitantes', 1, 'sca_visitante_salida_i.php', '', '', 'salida_visitantes.png', 0, 0),
(9, 0, 'Tipo de Visitante', 1, 'sca_submenu.php', '', '', 'tipo_visitantes.png', 4, 0),
(10, 9, 'Incluision de Tipo de Visitante', 1, 'sca_visitante_t_i.php', '', '', 'incluir_tipo_visitantes.png', 2, 0),
(11, 9, 'Consulta de Tipo de Visitante', 1, 'sca_visitante_t_c.php', '', '', 'consulta_tipo_visitantes.png', 1, 0),
(12, 5, 'Historial de Visitas', 1, 'sca_visitante_historial_c.php', '', '', 'historial_visitantes.png', 4, 0),
(13, 0, 'Principal', 3, 'sca_principal.php', '', '', 'principal.png', 1, 0),
(14, 0, 'Incluir Visitantes ', 3, 'sca_sno_personalnomina_c.php', '', '', 'incluir_visitantes.png', 2, 0),
(15, 0, 'Consulta Visitantes', 3, 'sca_visitante_c.php', '', '', 'consulta_visitantes.png', 3, 0),
(16, 0, 'Salida Visitantes', 3, 'sca_visitante_salida_i.php', '', '', 'salida_visitantes.png', 4, 0),
(17, 0, 'Historial de Visitas', 3, 'sca_visitante_historial_c.php', '', '', 'historial_visitantes.png', 5, 0),
(18, 0, 'Principal', 2, 'sca_principal.php', '', '', 'principal.png', 1, 0),
(19, 0, 'Inclusion de Usuario', 2, 'sca_adm_usuario_i.php', '', '', 'inclusion_usuario.png', 2, 0),
(20, 0, 'Modificacion de Usuarios', 2, 'sca_adm_usuario_m.php', '', '', 'modificacion_usuario.png', 3, 0),
(21, 0, 'Modificacion de Clave Usuarios', 2, 'sca_adm_usuario_clave_m.php', '', '', 'modificacion_clave_usuario.png', 4, 0),
(22, 0, 'Historial', 2, 'sca_adm_historial_c.php', '', '', 'historial.png', 5, 0),
(23, 0, 'Tipo de Correspondencia', 1, 'sca_submenu.php', '', '', 'tipo_correspondencia.png', 5, 0),
(24, 23, 'Consulta Tipo de Correspondencia', 1, 'sca_correspondencia_t_c.php', '', '', 'consulta_tipo_correspondencia.png', 1, 0),
(25, 23, 'Incluir Tipo de Correspondencia', 1, 'sca_correspondencia_t_i.php', '', '', 'incluir_tipo_correspondencia.png', 2, 0),
(26, 0, 'Tipo de Anexo', 1, 'sca_submenu.php', '', '', 'tipo_anexo.png', 6, 0),
(27, 26, 'Consulta Tipo de Anexo', 1, 'sca_anexo_t_c.php', '', '', 'consulta_tipo_anexo.png', 1, 0),
(28, 26, 'Incluir Tipo de Anexo', 1, 'sca_anexo_t_i.php', '', '', 'incluir_tipo_anexo.png', 2, 0),
(29, 0, 'Correspondencia', 1, 'sca_submenu.php', '', '', 'correspondencia.png', 7, 0),
(30, 29, 'Consulta Correspondencia', 1, 'sca_correspondencia_c.php', '', '', 'consulta_correspondencia.png', 1, 0),
(31, 29, 'Incluir Correspondencia', 1, 'sca_correspondencia_i.php', '', '', 'incluir_correspondencia.png', 2, 0),
(32, 29, 'Historial de Correspondencia', 1, 'sca_correspondencia_historial_c.php', '', '', 'historial_correspondencia.png', 3, 0),
(33, 0, 'Configuración BD', 2, 'sca_submenu.php', '', '', 'configuracion_bd.png', 6, 0),
(34, 33, 'Configuración BD Sigesp', 2, 'sca_sno_personal_tabla_c.php', '', '', 'configuracion_bd_sigesp.png', 1, 0),
(35, 33, 'Configuración BD SISYC', 2, 'sca_mincul_corr_tabla_c.php', '', '', 'configuracion_bd_sisyc.png', 2, 0),
(36, 5, 'Incluir Visitantes Básico', 1, 'sca_basico_sno_personalnomina_c.php', '', '', 'incluir_visitante_basico.png', 5, 1),
(37, 5, 'Completar Registro Visitante Básico', 1, 'sca_basico_visitante_carnet_i.php', '', '', 'completar_registro_visitante_basico.png', 6, 1),
(38, 0, 'Consulta Correspondencia', 4, 'sca_correspondencia_c.php', '', '', 'consulta_correspondencia.png', 2, 0),
(39, 0, 'Incluir Correspondencia', 4, 'sca_correspondencia_i.php', '', '', 'incluir_correspondencia.png', 3, 0),
(40, 0, 'Historial de Correspondencia', 4, 'sca_correspondencia_historial_c.php', '', '', 'historial_correspondencia.png', 4, 0),
(41, 0, 'Principal', 4, 'sca_principal.php', '', '', 'principal.png', 1, 0),
(42, 0, 'Principal', 5, 'sca_principal.php', '', '', 'principal.png', 1, 0),
(43, 0, 'Incluir Visitantes ', 5, 'sca_sno_personalnomina_c.php', '', '', 'incluir_visitantes.png', 2, 0),
(44, 0, 'Consulta Visitantes', 5, 'sca_visitante_c.php', '', '', 'consulta_visitantes.png', 3, 0),
(45, 0, 'Salida Visitantes', 5, 'sca_visitante_salida_i.php', '', '', 'salida_visitantes.png', 4, 0),
(46, 0, 'Historial de Visitas', 5, 'sca_visitante_historial_c.php', '', '', 'historial_visitantes.png', 5, 0),
(47, 0, 'Incluir Correspondencia', 5, 'sca_correspondencia_i.php', '', '', 'incluir_correspondencia.png', 6, 0),
(48, 0, 'Consulta Correspondencia', 5, 'sca_correspondencia_c.php', '', '', 'consulta_correspondencia.png', 7, 0),
(49, 0, 'Historial de Correspondencia', 5, 'sca_correspondencia_historial_c.php', '', '', 'historial_correspondencia.png', 8, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas_egresadas`
--

CREATE TABLE `personas_egresadas` (
  `id` int(11) NOT NULL,
  `cedula` varchar(20) NOT NULL,
  `nombre_y_apellido` varchar(225) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` varchar(100) NOT NULL,
  `clave` varchar(40) NOT NULL,
  `id_tipo_u` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `fecha_acceso_u` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip_acceso_u` varchar(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `clave`, `id_tipo_u`, `nombre`, `apellido`, `fecha_acceso_u`, `ip_acceso_u`) VALUES
('correspondencia', '7c4a8d09ca3762af61e59520943dc26494f8941b', 4, 'Correspondencia', 'Correspondencia', '2009-07-17 07:02:25', '172.16.226.250'),
('demo', '89e495e7941cf9e40e6980d14a16bf023ccd4c91', 1, 'demo', 'demo_2', '2016-05-25 13:40:24', '192.168.118.84'),
('demo2', 'db61277694fea41b42a8ac82cbb678baac683990', 3, 'demo2_nombre', 'demo2_apellido', '2022-06-09 13:40:23', '127.0.0.1'),
('demo3', '94fab99d6ca8abab7d217af7e3c5532697bf1a7a', 2, 'demo3', 'demo3', '2022-06-09 13:42:27', '127.0.0.1'),
('demo4', '3fb55a726b74dfbe73ae5d6de38b36166212c007', 5, 'demo4_nombre', 'demo4_apellido', '2011-10-03 17:57:10', '127.0.0.1'),
('hans', '2ba0be05531dac55c23f715871a1838c827f8bab', 2, 'Hans', 'Choque Gutierrez', '2022-06-23 19:07:07', '127.0.0.1'),
('mvera', '7c4a8d09ca3762af61e59520943dc26494f8941b', 3, 'Alejandra', 'Vera Quispe', '2022-06-23 18:45:18', '127.0.0.1'),
('vramos', '7e6545d0d90f62cc156752573f3f19887885e1da', 3, 'culturas', 'culturas', '2012-04-11 09:51:00', '127.0.0.1'),
('culturas', '7e6545d0d90f62cc156752573f3f19887885e1da', 3, 'culturas', 'culturas', '2018-09-20 09:26:40', '192.168.113.253');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_permisos`
--

CREATE TABLE `usuarios_permisos` (
  `id_usuario` varchar(100) NOT NULL,
  `url` varchar(255) NOT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_t`
--

CREATE TABLE `usuarios_t` (
  `id_tipo_u` int(100) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `observacion` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios_t`
--

INSERT INTO `usuarios_t` (`id_tipo_u`, `nombre`, `observacion`) VALUES
(0, 'Tipo de Usuario', ''),
(1, 'Administrador', ''),
(2, 'Administrador Sistema', ''),
(3, 'Recepcion', ''),
(4, 'Correspondencia', ''),
(5, 'Recepcion y Correspondencia', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visitantes`
--

CREATE TABLE `visitantes` (
  `cedula` varchar(20) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `apellido` varchar(255) NOT NULL,
  `telefono_movil` varchar(255) NOT NULL,
  `cod_ent` varchar(100) NOT NULL,
  `cod_mun` varchar(100) NOT NULL DEFAULT '',
  `cod_parr` varchar(100) NOT NULL DEFAULT '',
  `cod_cp` varchar(100) NOT NULL DEFAULT '',
  `direccion` text NOT NULL,
  `id_visitante_t` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `visitantes`
--

INSERT INTO `visitantes` (`cedula`, `nombre`, `apellido`, `telefono_movil`, `cod_ent`, `cod_mun`, `cod_parr`, `cod_cp`, `direccion`, `id_visitante_t`) VALUES
('3452687', 'ALEJANDRO NORTHON', 'ZAPATA AVENDA?O', ' 0  7652895', '01', '', '', '', '', 2),
('56895', 'reynaldo', 'mamani', '7411455', '18', '', '', '', '', 9),
('5996083', 'ALBERTO', 'MAMANI FLORES', '17816216', '03', '', '', '', '', 4),
('2680923', 'JUAN JAVIER', 'VELASQUEZ  ALCAZAR', ' 0  0  0 ', '0', '', '', '', '', 2),
('7051045', 'JAVIER BERNARDO', 'MAMANI FLORES', ' 0 ', '0', '', '', '', '', 2),
('8319845', 'ROSALIA GABRIELA', 'MANCILLA GOZALVEZ', ' 0  0  0 ', '0', '', '', '', '', 2),
('9892975', 'FREDERIK', 'HUIRI ROJAS', ' 0  0  0 ', '0', '', '', '', '', 2),
('397064', 'VICTOR ', 'LAGUNA PARISACA', ' 0  0 ', '0', '', '', '', '', 2),
('2704117', 'MARCELO EDMUNDO', 'VELASQUEZ TRIGO', ' 0  0  0 ', '0', '', '', '', '', 2),
('9865649', 'LUCERO YHAZMIN', 'GUERRERO TAPIA', ' 0  0  0  0 ', '28', '', '', '', '', 2),
('5475360', 'EDWIN', 'YUJRA QUISBERH', '7859854', '04', '', '', '', '', 2),
('4855183', 'CESAR LEONARDO', 'CONDORI OVIEDO', ' 0 ', '0', '', '', '', '', 2),
('6868325', 'DENNIS OMAR', 'ESCOBAR AVILA', ' 0  0  0  0  0 ', '0', '', '', '', '', 2),
('6122339', 'JOSE ADRIAN ', 'ROJAS CHAVEZ', ' 0  0 ', '0', '', '', '', '', 2),
('3482472', 'JAVIER JOAQUIN RONNY', 'BALDIVIOSO COLLAO', ' 0 ', '0', '', '', '', '', 2),
('6808980', 'FABIOLA ', 'APAZA TICONA', ' 0 ', '0', '', '', '', '', 2),
('1034788', 'BLANCA', 'MATIENZO PORTUGAL', ' 0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0 0000000000000000000000000000000000000', '0', '', '', '', '', 2),
('4751136', 'JUAN ALEJANDRO', 'BELTRAN CONDE', ' 0 00000000000000000000000000000000', '0', '', '', '', '', 2),
('4825765', 'NICOLE REYNA', 'FORTUN VARGAS', '000000000000000000000000000000000000', '0', '', '', '', '', 2),
('3499559', 'FERNANDO JAVIER ', 'CALISAYA MARTINEZ', '000000000000000000000000000000000000', '0', '', '', '', '', 2),
('2318033', 'JOHNY VENANCIO', 'ROJAS AYALA', '0000000000000000000000000000', '0', '', '', '', '', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visitantes_a`
--

CREATE TABLE `visitantes_a` (
  `id_visitante_h` int(11) NOT NULL,
  `base_db` varchar(255) NOT NULL,
  `codemp` char(4) NOT NULL,
  `autorizacion_nombre` varchar(255) NOT NULL,
  `codper` char(10) NOT NULL,
  `cedper` varchar(10) NOT NULL,
  `nomper` varchar(60) NOT NULL,
  `apeper` varchar(60) NOT NULL,
  `minorguniadm` char(4) NOT NULL,
  `ofiuniadm` char(2) NOT NULL,
  `uniuniadm` char(2) NOT NULL,
  `depuniadm` char(2) NOT NULL,
  `prouniadm` char(2) NOT NULL,
  `desuniadm` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Visitantes Autorizacion';

--
-- Volcado de datos para la tabla `visitantes_a`
--

INSERT INTO `visitantes_a` (`id_visitante_h`, `base_db`, `codemp`, `autorizacion_nombre`, `codper`, `cedper`, `nomper`, `apeper`, `minorguniadm`, `ofiuniadm`, `uniuniadm`, `depuniadm`, `prouniadm`, `desuniadm`) VALUES
(92657, '', '', 'ERNESTO MACHICADO', '', '', '', '', '', '', '', '', '', ''),
(92658, '', '', 'JOSE ARUQUIPA', '', '', '', '', '', '', '', '', '', ''),
(92659, '', '', 'JOSE ARUQUIPA', '', '', '', '', '', '', '', '', '', ''),
(92660, '', '', 'JOSE ARUQUIPA', '', '', '', '', '', '', '', '', '', ''),
(92661, '', '', 'SR. TAHUI', '', '', '', '', '', '', '', '', '', ''),
(92662, '', '', 'LOYOLA VALLE', '', '', '', '', '', '', '', '', '', ''),
(92663, '', '', 'ISABEL POMA', '', '', '', '', '', '', '', '', '', ''),
(92664, '', '', 'ERNESTO MACHICADO', '', '', '', '', '', '', '', '', '', ''),
(92665, '', '', 'ERNESTO MACHICADO', '', '', '', '', '', '', '', '', '', ''),
(92666, '', '', 'ERNESTO MACHICADO', '', '', '', '', '', '', '', '', '', ''),
(92667, '', '', 'MAX EGUIVAR', '', '', '', '', '', '', '', '', '', ''),
(92668, '', '', 'ERNESTO MACHICADO', '', '', '', '', '', '', '', '', '', ''),
(92669, '', '', 'ERNESTO MACHICADO', '', '', '', '', '', '', '', '', '', ''),
(92670, '', '', 'ERNESTO MACHICADO', '', '', '', '', '', '', '', '', '', ''),
(92671, '', '', 'ERNESTO MACHICADO', '', '', '', '', '', '', '', '', '', ''),
(92672, '', '', 'ERNESTO MACHICAO', '', '', '', '', '', '', '', '', '', ''),
(92673, '', '', 'YANETH VILLEGAS', '', '', '', '', '', '', '', '', '', ''),
(92674, '', '', 'YANETH VILLEGAS', '', '', '', '', '', '', '', '', '', ''),
(92675, '', '', 'ANTONIO ARUQUIPA', '', '', '', '', '', '', '', '', '', ''),
(92676, '', '', 'KARIN HURTADO', '', '', '', '', '', '', '', '', '', ''),
(107024, '', '', 'SANTOS QUISPE', '', '', '', '', '', '', '', '', '', ''),
(107025, '', '', 'VLADIMIR TINTAYA', '', '', '', '', '', '', '', '', '', ''),
(107026, '', '', 'SANTOS QUISPE', '', '', '', '', '', '', '', '', '', ''),
(107027, '', '', 'SANTOS QUISPE', '', '', '', '', '', '', '', '', '', ''),
(107028, '', '', 'santos', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visitantes_h`
--

CREATE TABLE `visitantes_h` (
  `id_visitante_h` int(11) NOT NULL,
  `cedula` varchar(20) NOT NULL,
  `cod_procedencia` varchar(255) NOT NULL,
  `procedencia` varchar(255) NOT NULL,
  `cod_emisor` varchar(255) NOT NULL,
  `emisor` varchar(255) NOT NULL,
  `fecha_e` datetime NOT NULL,
  `fecha_s` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_asunto` int(11) NOT NULL,
  `observacion` text NOT NULL,
  `id_carnet` varchar(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `visitantes_h`
--

INSERT INTO `visitantes_h` (`id_visitante_h`, `cedula`, `cod_procedencia`, `procedencia`, `cod_emisor`, `emisor`, `fecha_e`, `fecha_s`, `id_asunto`, `observacion`, `id_carnet`) VALUES
(92657, '3452687', '', 'LP', '', 'ASESORIA ', '2017-03-14 16:45:35', '2017-03-14 20:04:33', 11, '', '01M'),
(92658, '5996083', '', 'LP', '', 'PROMOCION CULTURAL', '2017-03-14 16:47:34', '2017-03-14 20:04:38', 11, '', '02M'),
(92659, '2680923', '', 'LP', '', 'PROMOCION CULTURAL', '2017-03-14 16:49:27', '2017-03-14 20:04:42', 11, '', '03M'),
(92660, '7051045', '', 'LP', '', 'PROMOCION CULTURAL', '2017-03-14 16:51:04', '2017-03-14 20:04:49', 11, '', '04M'),
(92661, '8319845', '', 'LP', '', 'COMUNICACIONES', '2017-03-14 16:52:51', '2017-03-14 20:04:57', 11, '', '05M'),
(92662, '9892975', '', 'LP', '', 'ALMACENES', '2017-03-14 17:00:08', '2017-03-14 20:05:07', 11, '', '06M'),
(92663, '397064', '', 'LP', '', 'AUDITORIA ', '2017-03-14 17:24:06', '2017-03-14 20:05:12', 11, '', '07M'),
(92664, '2704117', '', 'LP', '', 'ASESORIA', '2017-03-14 17:29:10', '2017-03-14 20:05:17', 11, '', '08M'),
(92665, '9865649', '', 'LP', '', 'DESPACHO', '2017-03-14 17:30:58', '2017-03-14 20:05:22', 11, '', '09M'),
(92666, '5475360', '', 'LP', '', 'DESPACHO', '2017-03-14 17:32:23', '2017-03-14 20:05:26', 11, '', '10M'),
(92667, '4855183', '', 'LP', '', 'PROMOCION CULTURAL', '2017-03-14 17:42:22', '2017-03-14 20:05:32', 11, '', '11M'),
(92668, '6868325', '', 'LP', '', 'DESPACHO', '2017-03-14 19:23:51', '2017-03-14 20:05:38', 11, '', '12M'),
(92669, '6122339', '', 'LP', '', 'DESPACHO', '2017-03-14 19:25:15', '2017-03-14 20:05:43', 11, '', '13M'),
(92670, '3482472', '', 'LP', '', 'DESPACHO', '2017-03-14 19:27:04', '2017-03-14 20:05:48', 11, '', '14M'),
(92671, '6808980', '', 'LP', '', 'DESPACHO', '2017-03-14 19:52:35', '2017-03-14 20:05:52', 11, '', '15M'),
(92672, '1034788', '', 'LA PAZ', '', 'DESPACHO', '2017-03-15 10:01:16', '2017-03-15 10:16:59', 11, '', '01M'),
(92673, '4751136', '', 'LA PAZ', '', 'RADIO CULTURAS', '2017-03-15 10:03:12', '2017-03-15 12:25:34', 11, '', '02M'),
(92674, '4825765', '', 'LA PAZ', '', 'RADIO CULTURAS', '2017-03-15 10:05:35', '2017-03-15 12:25:38', 11, '', '03M'),
(92675, '3499559', '', 'LA PAZ', '', 'PROMOCION CULTURAL', '2017-03-15 10:33:18', '2017-03-15 12:25:41', 11, '', '04M'),
(92676, '2318033', '', 'LA PAZ', '', 'RECURSOS HUMANOS', '2017-03-15 10:34:26', '2017-03-15 12:25:45', 11, '', '05M'),
(107024, '3452687', '', 'LA PAZ', '', 'DESARROLLO', '2022-06-09 18:09:25', '2022-06-09 18:10:00', 1, 'REUNION DE COORDINACION DE DERMALOG', '10'),
(107025, '3452687', '', 'ORURO', '', 'BASE DE DATOS', '2022-06-09 18:25:54', '2022-06-09 18:37:45', 3, 'REUNION DE VERIFICAICON DE SERVICIO', '5'),
(107026, '5475360', '', 'La paz', '', 'DESARROLLO', '2022-06-09 18:40:16', '2022-06-09 18:56:45', 6, 'REUNION DE COORDINACION', '8'),
(107027, '5996083', '', 'agasdgasd', '', 'DESARROLLO', '2022-06-09 14:49:34', '2022-06-09 14:49:56', 4, 'REUNION DE COORDINACION', '7'),
(107028, '56895', '', 'depa la paz', '', 'desarrollo', '2022-06-23 18:46:42', '2022-06-23 18:46:56', 8, 'SISTEMA DE RRHH', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visitantes_t`
--

CREATE TABLE `visitantes_t` (
  `id_visitante_t` int(2) NOT NULL,
  `tipo` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `visitantes_t`
--

INSERT INTO `visitantes_t` (`id_visitante_t`, `tipo`) VALUES
(0, '---seleccione---'),
(1, 'Particular'),
(2, 'Proveedor de Servicios'),
(3, 'Familiar de Funcionario'),
(4, 'Funcionario del SEGIP'),
(5, 'Funcionario Publico de Otra Institucion'),
(6, 'Medio de Comunicacion'),
(7, 'Representante del Sector Privado'),
(8, 'Representante de Cuerpo Diplomatico'),
(9, 'Soporte Tecnico Externo');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `anexos_t`
--
ALTER TABLE `anexos_t`
  ADD PRIMARY KEY (`id_anexo_t`);

--
-- Indices de la tabla `asuntos`
--
ALTER TABLE `asuntos`
  ADD PRIMARY KEY (`id_asunto`);

--
-- Indices de la tabla `bases_db`
--
ALTER TABLE `bases_db`
  ADD PRIMARY KEY (`base_db`,`id_base_db_t`),
  ADD KEY `id_base_db_t` (`id_base_db_t`);

--
-- Indices de la tabla `bases_db_t`
--
ALTER TABLE `bases_db_t`
  ADD PRIMARY KEY (`id_base_db_t`);

--
-- Indices de la tabla `correspondencias`
--
ALTER TABLE `correspondencias`
  ADD PRIMARY KEY (`id_correspondencia`);

--
-- Indices de la tabla `correspondencias_t`
--
ALTER TABLE `correspondencias_t`
  ADD PRIMARY KEY (`id_correspondencia_t`);

--
-- Indices de la tabla `empcp`
--
ALTER TABLE `empcp`
  ADD PRIMARY KEY (`cod_ent`,`cod_mun`,`cod_parr`,`cod_cp`),
  ADD KEY `cod_cp` (`cod_cp`),
  ADD KEY `cod_parr` (`cod_parr`),
  ADD KEY `cod_mun` (`cod_mun`),
  ADD KEY `cod_ent` (`cod_ent`);

--
-- Indices de la tabla `historiales`
--
ALTER TABLE `historiales`
  ADD PRIMARY KEY (`id_historial`);

--
-- Indices de la tabla `historiales_m`
--
ALTER TABLE `historiales_m`
  ADD PRIMARY KEY (`id_historial_m`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indices de la tabla `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id_menu`),
  ADD KEY `id_tipo_u` (`id_tipo_u`);

--
-- Indices de la tabla `personas_egresadas`
--
ALTER TABLE `personas_egresadas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cedula` (`cedula`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`),
  ADD KEY `id_tipo_u` (`id_tipo_u`);

--
-- Indices de la tabla `usuarios_permisos`
--
ALTER TABLE `usuarios_permisos`
  ADD PRIMARY KEY (`id_usuario`,`url`);

--
-- Indices de la tabla `usuarios_t`
--
ALTER TABLE `usuarios_t`
  ADD PRIMARY KEY (`id_tipo_u`);

--
-- Indices de la tabla `visitantes`
--
ALTER TABLE `visitantes`
  ADD PRIMARY KEY (`cedula`);

--
-- Indices de la tabla `visitantes_a`
--
ALTER TABLE `visitantes_a`
  ADD PRIMARY KEY (`id_visitante_h`);

--
-- Indices de la tabla `visitantes_h`
--
ALTER TABLE `visitantes_h`
  ADD PRIMARY KEY (`id_visitante_h`);

--
-- Indices de la tabla `visitantes_t`
--
ALTER TABLE `visitantes_t`
  ADD PRIMARY KEY (`id_visitante_t`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `anexos_t`
--
ALTER TABLE `anexos_t`
  MODIFY `id_anexo_t` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `asuntos`
--
ALTER TABLE `asuntos`
  MODIFY `id_asunto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `bases_db_t`
--
ALTER TABLE `bases_db_t`
  MODIFY `id_base_db_t` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `correspondencias`
--
ALTER TABLE `correspondencias`
  MODIFY `id_correspondencia` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `correspondencias_t`
--
ALTER TABLE `correspondencias_t`
  MODIFY `id_correspondencia_t` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `historiales`
--
ALTER TABLE `historiales`
  MODIFY `id_historial` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=428816;
--
-- AUTO_INCREMENT de la tabla `historiales_m`
--
ALTER TABLE `historiales_m`
  MODIFY `id_historial_m` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT de la tabla `menus`
--
ALTER TABLE `menus`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT de la tabla `personas_egresadas`
--
ALTER TABLE `personas_egresadas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `usuarios_t`
--
ALTER TABLE `usuarios_t`
  MODIFY `id_tipo_u` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `visitantes_h`
--
ALTER TABLE `visitantes_h`
  MODIFY `id_visitante_h` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107029;
--
-- AUTO_INCREMENT de la tabla `visitantes_t`
--
ALTER TABLE `visitantes_t`
  MODIFY `id_visitante_t` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
