# MANUAL DE INSTALACIÓN
## Requerimientos preliminales
> Este proceso se verificó en un entorno Linux con distribución Debian 10

### Instalación del servidor web Apache

#### Paso 1

Actualizar el repositorio interno del sistema operativo

```bash
sudo apt update
```
#### Paso 2

Instalar el servidor apache 2

```bash
sudo apt install apache2
```
#### Paso 3

Para verificar que el servidor web esta en line puede utilizar el comando
```bash
ss -lntu
```
*Ejempo de salida*

    Netid        State         Recv-Q        Send-Q               Local Address:Port               Peer Address:Port
    tcp          LISTEN        0             128                        0.0.0.0:22                      0.0.0.0:*
    tcp          LISTEN        0             128                              *:80                            *:*
    tcp          LISTEN        0             128                           [::]:22                         [::]:*


y verificar que el puerto 80 de su servidor esta activo

### Instalación del PHP 5.6

#### Paso 1
Se debe actualizar la lista de repositorios con el siguiente comando:

```bash
sudo apt update
```
#### Paso 2
Agregar los repositorios de PPA de Sury.

```bash
sudo apt -y install lsb-release apt-transport-https ca-certificates 
sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
```

### Paso 3
Agregar el repositorio a la lista de repositorios del servidor, seguido esto actualizamos la lista de paquetes.

```bash
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php.list

sudo apt update
```

### Paso 4
Proceder a instalar PHP 5.6 en el sistema
```bash
sudo apt install php5.6 libapache2-mod-php5.6
```

### Paso 5
Verificar la versión del PHP instalada

```bash
php -v
```

*Ejempo de salida*

    PHP 5.6.40-60+0~20220627.67+debian10~1.gbp1f7ffd (cli)
    Copyright (c) 1997-2016 The PHP Group
    Zend Engine v2.6.0, Copyright (c) 1998-2016 Zend Technologies
        with Zend OPcache v7.0.6-dev, Copyright (c) 1999-2016, by Zend Technologies

### Paso 6

Instalar las extensiones necesarias para el sistema

```bash
sudo apt install php5.6-mysqli
sudo apt install php5.6-xml
```

### Paso 7
Reiniciar el servidor apache.

```bash
sudo service apache2 restart
```
### Instalación del servidor de base de datos MariaDB

#### Paso 1
Instalar MariaDB directamente de los repositorios de DEBIAN

```bash
sudo apt update
sudo apt install mariadb-client mariadb-server
```

#### Paso 2 
Verificar el estado del servidor mariadb
```bash
sudo systemctl status mariadb
```

Si el servicio esta inactivo, se puede activar con el siguiente comando
```bash
sudo systemctl start mariadb
sudo systemctl status mariadb
```

Adicionar el servicio para que inicie en forma automatica
```bash
sudo systemctl enable mariadb
```

#### Paso 3
Asignar las contraseñas de super usuario de MariaDB con el siguiente comando.
```bash
sudo mysql_secure_installation
```

#### Paso 4
Comprobar que se pueda acceder al servidor de base de datos MariaDB con el usuario root
```bash
sudo mysql -u root -p
```

*Ejemplo de salida*

    Welcome to the MySQL monitor.  Commands end with ; or \g.
    Your MariaDB connection id is 40
    Server version: 10.3.34-MariaDB-0+deb10u1 Debian 10

    Copyright (c) 2000, 2018, Oracle, MySQL Corporation Ab and others.

    Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

    MariaDB [(none)]>

Salir del servidor

```sql
exit
```

### Copiar el código del sistema

#### Paso 1
Clonar el codigo fuente del sistema.

```bash
git clone -b master https://gitlab.com/JhonnyMonrroy/sistema-de-control-de-accesos.git sca
```

#### Paso 2
Mover la carpeta al directorio */var/www/* del Apache2.

```bash
sudo mv sca /var/www/
```

### Paso 3
Poner como propietario al usuario ROOT para que apache tenga accesos de lectura y escritura
```bash
sudo chown -R root:root /var/www/sca
```
Asignar permisos de lectura y escritura

```bash
sudo chmod 647 /var/www/sca/ -R
```

#### Paso 4
Modificar el archivo */etc/apache2/sites-available/000-default.conf* para redireccionar a la pagina principal al sistema sca.

```bash
sudo nano /etc/apache2/sites-available/000-default.conf
```

Buscar el apartado *DocumentRoot /var/www/html* y cambiarlo por **_DocumentRoot /var/www/sca_**

```ini
. . .
    # However, you must set it for any further virtual host explicitly.
    #ServerName www.example.com

    ServerAdmin webmaster@localhost
    # DocumentRoot /var/www/html
    DocumentRoot /var/www/sca

    # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
    # error, crit, alert, emerg.

. . .
```

Cerrar y Guardar el archivo con *Ctrl + o*  y  *Ctrl + x*.

#### Paso 5
Reiniciar el servidor apache.

```bash
sudo service apache2 restart
```

El sistema se deberia desplegar correctamente.

![](doc/images/inst_001.png)

## Configuración de la Base de datos
El software *Sistema de Control de Accesos* trabaja con el gestor de base de datos *MySQL* compatible con *MariaDB* 

### Paso 1
Igresar con sus credenciales de MySQL en el servidor de MySQL.

```bash
mysql -u <username> -p
```
### Paso 2
Dentro de la interfaz de MySQL se debe adicionar al usuario que hara uso del sistema.

```sql
CREATE USER 'sca_user'@'%' IDENTIFIED BY '<password>';
```

### Paso 3
Crear la base de datos
```sql
CREATE DATABASE sca_db CHARACTER SET utf8;
```

### Paso 4
Se le asigna los correspondientes permisos al usuario para acceder a la base de datos.
```sql
GRANT ALL ON sca_db.* TO 'sca_user'@'%';

FLUSH PRIVILEGES;

exit
```

### Restaurar el backup de la base de datos inicial.

#### Paso 1

Para restaurar el backup inicial de la base de datos ejecutar el comando.

```bash
mysql -u sca_user -p sca_db < /var/www/sca/DB_CARGAR/db_sca.sql
```
### Paso 2 

Copiar el archivo *_sca_funciones.php.sample_* y modificar los parametros segun corresponda.

```bash
sudo cp /var/www/sca/sca_funciones.php.sample /var/www/sca/sca_funciones.php
```

Se edita el archivo *_sca_funciones.php_* segun su configuración de base de datos

```bash
sudo nano /var/www/sca/sca_funciones.php
```

Cambiar los valores segun su configuración de base de datos

```php
//Inicio Funcion Conexion con la Base de Datos
function conectar()
	{
	$serv = "localhost";
	$user = "sca_user";
	$pass = "XXXXXXX";
	$mydb = "sca_db";//Nombre de la Base de datos
	$link = @mysql_connect($serv, $user, $pass); 
	@mysql_select_db($mydb, $link); 
	if (! $link)
		{
			$control =0;
		}
		else
		{
			$control=1;
		}
	return $control;
	}
//Fin Funcion Conexion con la Base de Datos

```

### Paso 3
Guardar el archivo y reiniciar el servidor apache 2

```bash
sudo service apache2 restart
```

### Paso 4
Una vez configurado el sistema ingrese con sus cuentas de usuario.

![](doc/images/inst_002.png)

# USUARIOS DE ACCESO

| Rol | Usuario | Password |
|---|---|---|
| Administrador | demo | demo |
| Recepción | demo2 | demo2 |
| Admin Sistema | demo3 | demo3 | 
| Correspondencia | demo4 | demo 4 |

## TIPOS DE USUARIOS habilitados

1. 	Administrador 	
2. 	Administrador Sistema 	
3. 	Recepcion 	
4. 	Correspondencia 	
5. 	Recepcion y Correspondencia 	

> Ultima modificación del manual 04/08/2022 por @JhonnyMonrroy


